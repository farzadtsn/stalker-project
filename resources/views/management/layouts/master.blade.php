<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('img/favicon.ico') }}">
    <link href="https://cdn.jsdelivr.net/npm/@mdi/font@4.x/css/materialdesignicons.min.css" rel="stylesheet">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{mix('css/app.css')}}" rel="stylesheet">
{{--    <link href="{{mix('css/management.css')}}" rel="stylesheet">--}}
    <title> داشبور استاکر </title>
</head>
<body>
<div id="content">
    <v-app style="background-color: #1e1e2f">
        @yield('content')
    </v-app>
</div>


<script src="{{mix('js/app.js')}}" ></script>
@yield('resources')

</body>
</html>
