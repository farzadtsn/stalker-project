<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('img/favicon.ico') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="https://cdn.jsdelivr.net/npm/@mdi/font@4.x/css/materialdesignicons.min.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/particlesjs/2.2.3/particles.js"></script>
    <link href="{{mix('css/app.css')}}" rel="stylesheet">
    <link href="{{mix('css/general.css')}}" rel="stylesheet">
    <title> سایت استاکر </title>
</head>
<body>
    <div id="content">
        <v-app>
            <headers></headers>
                <contents></contents>
            <footers></footers>
        </v-app>
    </div>



<script>
    particlesJS("particles-js", {
        "particles": {
            "number": {
                "value":185,
                "density": {
                    "enable":true
                    ,"value_area":1200
                }
            },
            "color":{
                "value":"#4b8bf1"
            },
            "shape":{
                "type":"circle",
                "stroke":{
                    "width":0,
                    "color":"#000000"
                },
                "polygon":{
                    "nb_sides":10
                },
                "image":{
                    "src":"img/github.svg",
                    "width":100,
                    "height":100
                }
            },
            "opacity":{
                "value":0.7,
                "random":false,
                "anim":{
                    "enable":true,
                    "speed":1,
                    "opacity_min":0.1,
                    "sync":true
                }
            },
            "size":{
                "value":5,
                "random":true,
                "anim":{
                    "enable":true,
                    "speed":50,
                    "size_min":0.1,
                    "sync":false
                }
            },
            "line_linked":{
                "enable":true,
                "distance":130,
                "color":"#26ceff",
                "opacity":0.5,
                "width":1
            },
            "move":{
                "enable":true,
                "speed":2,
                "direction":"none",
                "random":true,
                "straight":false,
                "out_mode":"out",
                "bounce":true,
                "attract":{
                    "enable":true,
                    "rotateX":1400,
                    "rotateY":1900
                }
            }
        },
        "interactivity":{
            "detect_on":"canvas",
            "events":{
                "onhover":{
                    "enable":false,
                    "mode":"bubble"
                },
                "onclick":{
                    "enable":true,
                    "mode":"repulse"
                },
                "resize":true
            },
            "modes":{
                "grab":{
                    "distance":400,
                    "line_linked":{
                        "opacity":1
                    }
                },
                "bubble":{
                    "distance":600,
                    "size":15,
                    "duration":2,
                    "opacity":8,
                    "speed":5
                },
                "repulse":{
                    "distance":200,
                    "duration":0.4
                },
                "push":{
                    "particles_nb":4
                },
                "remove":{
                    "particles_nb":3
                }
            }
        },
        "retina_detect":true
    });

    anime({
        targets: '.image_banner',
        translateY: 50,
        direction: 'alternate',
        loop: true,
        easing: 'cubicBezier(.7, .09, .3, .8)',
        duration: 2000
    });
</script>

<script src="{{mix('js/app.js')}}" ></script>
    @yield('resources')

</body>
</html>
