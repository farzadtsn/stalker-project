window._ = require('lodash');


try {
    window.Popper = require('popper.js').default;
    var $ = window.$ = window.jQuery = require('jquery');
    require('../js/plugins/js/jquery/jquery-ui-1.10.3.min.js');
    require('../js/plugins/js/bootstrap/bootstrap.min.js');
    const anime = require('animejs');

} catch (e) {}


window.axios = require('axios');
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';



let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}
