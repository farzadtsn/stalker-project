import Vue from 'vue';
import VueRouter from 'vue-router';
Vue.use(VueRouter);

/* ============================ Register User Page Component ============================*/
import Profile from '../components/management/users/ProfileComponent' ;
import List from '../components/management/users/ListComponent';

const routes = [
    { path : '/management/user/profile' , component : Profile },
    { path : '/management/user/list' , component : List }
];


const router= new VueRouter({
    routes ,
    hashbang : false,
    mode:'history'
}) ;

export default router ;
