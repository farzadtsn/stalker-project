require('./bootstrap');

import Vue from 'vue';

import Vuetify from 'vuetify';
import 'vuetify/dist/vuetify.min.css'
Vue.use(Vuetify);

import router from './router/router.js' ;

/* ======================== Register General Page Component =========================*/
Vue.component('Headers', require('./components/general/Header.vue').default);
Vue.component('Contents', require('./components/general/Content.vue').default);
Vue.component('Footers', require('./components/general/Footer.vue').default);

/* ======================== Register Management Page Component =========================*/
Vue.component('managementHeader', require('./components/management/HeaderComponent.vue').default);


const app = new Vue({
    vuetify : new Vuetify({
        rtl: true,
    }),
    el: '#content',
    router
});
