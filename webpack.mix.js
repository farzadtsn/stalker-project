const mix = require('laravel-mix');



mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .styles(['resources/css/general.css'] , 'public/css/general.css')
    // .styles(['resources/css/management.css'] , 'public/css/management.css')
    .copyDirectory('resources/images', 'public/images')
    .sourceMaps()
    .version();
