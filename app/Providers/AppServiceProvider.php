<?php

namespace App\Providers;


use App\Models\Language;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(){
                if(!app()->runningInConsole()) {
            if(Request::isMethod('get')) {
                $languages = Language::where("status", "active")->get(["caption", "language"]);
                view()->share("__languages", $languages);
            }
        }


        if (preg_match("/(install)/", Request::getRequestUri())) {
            return;
        }

        if (Request::wantsJson()) {
            return;
        }
    }
}
