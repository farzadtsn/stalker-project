<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Gate;
use App\Models\Permission;

class AuthServiceProvider extends ServiceProvider{


    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];




    public function boot(){
        $this->registerPolicies();

        if(preg_match("/(install)/" ,Request::getRequestUri())) {
            return;
        }

//        if(!app()->runningInConsole()) {
//            foreach ($this->getPermissions() as $permission) {
//                Gate::define($permission->value, function ($user) use ($permission) {
//                    return $user->hasRole($permission->roles); // return Object : Array
//                });
//            }
//        }
    }

    protected function getPermissions(){
        return Permission::with('roles')->get();
    }
}
