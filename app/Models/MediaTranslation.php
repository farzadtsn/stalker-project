<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MediaTranslation extends Model
{
    protected $table = "media_translation";
    protected $fillable = ["media_id" , "caption" , "description" , "language"];
}
