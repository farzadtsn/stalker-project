<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use App\Models\User ;
use App\Models\Permission ;

class Role extends Model
{
    protected $table = "roles";
    protected $fillable = ['status','value'];

//    protected $casts=[
//        'permission_id'=>'array',
//    ];

    public function translations(){
        return $this->hasMany(RoleTranslation::class);
    }

    public function translate(){
        return $this->hasOne(RoleTranslation::class)->where("language" , App::getLocale());
    }

    public function users(){
        return $this->belongsToMany(User::class);
    }


    public function permissions(){
        return $this->belongsToMany(Permission::class);
    }

    
}
