<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Tag extends Model{

    protected $table = 'tags';


    protected $fillable = ['value' , 'kind' , 'status'];


    public function translations(){
        return $this->hasMany(TagTranslation::class);
    }


    public function translate(){
        return $this->hasOne(TagTranslation::class)->where("language" , App::getLocale());
    }


    public function medias(){
        return $this->hasMany(Media::class);
    }

}
