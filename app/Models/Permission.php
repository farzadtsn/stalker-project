<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App ;
use App\Models\Role ;

class Permission extends Model{

    protected $table = "permissions";
    protected $fillable = ['value','status'];


    public function translations(){
        return $this->hasMany(PermissionTranslation::class);
    }

    public function translate(){
        return $this->hasOne(PermissionTranslation::class)->where("language" , App::getLocale());
    }


    public function roles(){
        return $this->belongsToMany(Role::class);
    }
}
