<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Category extends Model
{
    protected $table = 'categories';
    protected $fillable = ['value' , 'kind' , 'status'];

    public function translations(){
        return $this->hasMany(CategoryTranslation::class);
    }


    public function translate(){
        return $this->hasOne(CategoryTranslation::class)->where("language" , App::getLocale());
    }


    public function medias(){
        return $this->hasMany(Media::class);
    }



    public function news(){
        return $this->hasMany(News::class);
    }



    public function events(){
        return $this->hasMany(Event::class);
    }
}
