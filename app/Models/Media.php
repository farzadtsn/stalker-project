<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Media extends Model
{
    protected $table = 'medias';
    protected $fillable = ['value' , 'postfix' , 'kind' , 'category_id' , 'model_id' , 'model_type' , 'type' , 'status'];

    public function translate(){
        return $this->hasOne(MediaTranslation::class)->where("language" , App::getLocale());
    }

    public function translations(){
        return $this->hasMany(MediaTranslation::class);
    }

    public function category(){
        return $this->belongsTo(Category::class);
    }

    public function pages(){
        return $this->hasMany(Page::class);
    }

    public function events(){
        return $this->hasMany(Event::class);
    }

    public function branches(){
        return $this->belongsToMany(Branch::class);
    }
}
