<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TagTranslation extends Model{

    protected $table = "tag_translation";


    protected $fillable = ["tag_id" , "caption" , "language"];
}
