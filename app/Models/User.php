<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable{
    use Notifiable;

    protected $table = "users";


    protected $fillable = [
        'firstName','lastName','email','password','mobileNumber','country','city','birthDate','level','status'
    ];



    protected $hidden = [
        'password', 'remember_token',
    ];



    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getFullNameAttribute() {
        return "{$this->firstName} {$this->lastName}";
    }


    protected $appends = [
        'full_name'
    ];
}
