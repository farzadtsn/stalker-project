<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PermissionTranslation extends Model{

    protected $table = "permission_translation";
    protected $fillable = ["permission_id" , "caption", "language"];

}
