<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RoleTranslation extends Model{

    protected $table = "role_translation";
    protected $fillable = ["role_id" , "caption", "language"];
}
