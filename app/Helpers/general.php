<?php

if(!function_exists("translate")) {
    function translate($value , $property , $get = "string") {
        if(isset($value->{$property})) {
            return $value->{$property};
        }

        if($get === "string") {
            return trans("general.translate.error");
        }
        return false;
    }
}
