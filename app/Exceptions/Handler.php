<?php

namespace App\Exceptions;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Handler extends ExceptionHandler{

    protected $dontReport = [

    ];



    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];



    public function report(Throwable $exception){
        parent::report($exception);
    }


    public function render($request, Throwable $exception){

        $array = explode("/" , $exception->getFile());
        $first = array_search("app" , $array);

        if($request->wantsJson()) {
            if ($exception instanceof ModelNotFoundException) {
                $exception = new NotFoundHttpException($exception->getMessage(), $exception);
            }
            if(method_exists($exception, 'getStatusCode')) {
                $statusCode = $exception->getStatusCode();
                switch ($statusCode) {
                    case "404":
                        return response()->json(["ok" => false , "code" => 404 , "errors" => ["404" => [trans("errors.resource.404")]]]);
                    default:
                        return response()->view("errors.errors" , ["code" => $statusCode , "message" => "Unknown Error ..."]);
                }
            } else {
                return response()->json(["ok" => false , "errors" => ["message" => [$exception->getMessage()] , "file" => [implode("/" , array_slice($array , $first))] , "line" => [$exception->getLine()]]]);
            }
        } else {
            if(method_exists($exception, 'getStatusCode')) {
                $statusCode = $exception->getStatusCode();
                switch ($statusCode) {
                    case "404":
                        return response()->view("errors.errors" , ["code" => $statusCode , "message" => "page not found"]);
                    case "405":
                        return response()->view("errors.errors" , ["code" => $statusCode , "message" => "cannot get"]);
                    default:
                        return response()->view("errors.errors" , ["code" => $statusCode , "message" => "Unknown Error ..."]);
                }
            }
            return parent::render($request, $exception);
        }

    }
}
