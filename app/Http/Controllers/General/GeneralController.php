<?php

namespace App\Http\Controllers\General;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;

class GeneralController extends Controller{

    public function config(){
        Artisan::call('view:clear');
        Artisan::call('config:cache');
        Artisan::call('key:generate');
        Artisan::call('migrate:reset');
            echo Artisan::output() . "</br></br>";
        Artisan::call('migrate');
            echo Artisan::output() . "</br></br>";
        Artisan::call('db:seed');
            echo Artisan::output() . "</br></br>";
    }

    public function home(){
        return view('layouts.master');
    }
}
