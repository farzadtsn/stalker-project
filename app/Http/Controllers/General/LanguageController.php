<?php

namespace App\Http\Controllers\General;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Symfony\Component\Console\Input\Input;

class LanguageController extends Controller{

    public function change(Request $request){
        $this->validate($request,[
            'lang' => 'required|string|min:2|max:2|exists:languages,language'
        ]);
        Cookie::queue('locale', Input::get('lang') , 3153600000);
        return redirect()->back();
    }
}
