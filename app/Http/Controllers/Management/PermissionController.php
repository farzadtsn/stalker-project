<?php

namespace App\Http\Controllers\Management;


use App\Models\Language;
use App\Models\Permission;
use App\Models\PermissionTranslation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB ;
use App\Http\Controllers\Controller;

class PermissionController extends Controller{

    public function show() {
        $permissions=Permission::with(['translate'])->latest()->get();
    //    $permissions->dump();
    //    return $permissions ;
        return view('management.permission.list',compact('permissions'));
    }


    public function create(){
        return view('management.permission.create');
    }


    public function store(Request $request){

        $this->validate($request, [
            'value' => 'required|min:2|max:255|regex:/^[a-z ,.\'-]+$/i',
            'caption' => 'required|min:2|max:255|string'
        ]);

        DB::beginTransaction();
        try {
            $permission = Permission::create([
                'value' => $request->value ,
            ]);
            if ($permission) {
                $permissionTranslation=PermissionTranslation::create([
                    "permission_id" => $permission->id,
                    "caption" => $request->caption,
                    "language" => "fa",
                ]);
                if($permissionTranslation){
                    DB::commit();
                    return redirect()->route('management.permission.list')->with(['ok' => true]);
                }
            }
        } catch (\Exception $exception) {
            DB::rollBack();
        }
        return redirect()->route('management.permission.create')->with(["ok" => false]);
    }



    public function edit(Permission $permission){
        return response()->json(['ok' => true, 'permission' => $permission->toArray(), 'translations' => $permission->translations->toArray()]);
    }



    public function update(Permission $permission, $language, Request $request){
        $lang = false;
        if (is_numeric($language)) {
            $lang = Language::find($language);
        } else {
            $lang = Language::where([["language", $language], ["status", true]]);
        }
        if (!$lang) {
            abort(404);
        } else if ($lang->count() < 1) {
            abort(404);
        }
        $validator = validator($request->toArray(), [
            'caption' => 'required|min:3|max:255|string',
            'value' => 'min:3|max:255|string|unique:permissions,value|regex:/[a-z\/-0-9A-Z ]+/'.$permission->id,
            'status' => 'in:active,inactive,deleted',
        ]);
        if ($validator->fails()) {
            return response()->json(["ok" => false, "errors" => $validator->errors()]);
        }
        $lang = $lang->first();

        DB::beginTransaction();
        try {

            $PermissionTranslation = PermissionTranslation::where([['permission_id', $permission->id], ['language', $lang->language]]);
//                dd($PermissionTranslation);
            if($PermissionTranslation->count() > 0) {
                $PermissionTranslation->update([
                    'caption' => $request->caption,
                ]);
            }
            else {
                PermissionTranslation::create([
                    "permission_id" => $permission->id,
                    "caption" => $request->caption,
                    "language" => $lang->language,
                ]);

            }


            $PermissionTranslation = PermissionTranslation::where([['permission_id', $permission->id], ['language', $lang->language]]);

            if ($PermissionTranslation) {
                $fields = [];
                $updatePermission  = false;
                if($request->has("value")) {
                    $value = str_slug($request->value);
                    $fields["value"]  = $request->$value;
                    $updatePermission = true;
                }

                if($request->has("status")) {
                    $fields["status"]  = $request->status;
                    $updatePermission = true;
                }
                if($updatePermission) {
                    $updatePermission = $permission->update($fields);
                    if ($updatePermission) {
                        DB::commit();
                        return response()->json(['ok' => true, "data" => ["permission" => $permission->toArray(), "translations" => $permission->translations->toArray()]]);
                    }
                }
                DB::commit();
                return response()->json(['ok' => true, "data" => ["permission" => $permission->toArray(), "translations" => $permission->translations->toArray()]]);
            }
            DB::rollBack();

        } catch (\Exception $exception) {

            DB::rollBack();
        }
        return response()->json(['ok' => false]);

//        $lang = false;
//        if (is_numeric($language)) {
//            $lang = Language::find($language);
//        } else {
//            $lang = Language::where([["language", $language], ["status", true]]);
//        }
//        if (!$lang) {
//            abort(404);
//        } else if ($lang->count() < 1) {
//            abort(404);
//        }
//        $validator = validator($request->toArray(), [
//            'caption' => 'required|min:5|max:255|string',
//            'value' => 'required|min:2|max:255|unique:permissions,value|regex:/^[a-z ,.\'-]+$/i'. $permission->id,
//        ]);
//        if ($validator->fails()) {
//            return response()->json(["ok" => false, "errors" => $validator->errors()]);
//        }
//
//        $lang = $lang->first();
//        DB::beginTransaction();
//        try {
//            $permissions= $permission->update($request->all());
//
//            foreach ($permission->translations as $translation) {
//                $permissionTranslation = PermissionTranslation::where([['permission_id', $permission->id], ['language', $translation->language]]);
//                if($permissionTranslation->count() < 1) {
//                    PermissionTranslation::create([
//                        "permission_id" => $permission->id,
//                        "caption" => $translation->caption,
//                        "language" => $translation->language,
//                    ]);
//                }
//                elseif ($permissionTranslation->count() > 0) {
//                    $permissionTranslationUpdate = $permissionTranslation->update([
//                        'caption' => $request->caption,
//                    ]);
//                    $permissionTranslation->first();
//                }
//            }
//
//            $permissionTranslation = PermissionTranslation::where([['permission_id', $permission->first()->id], ['language', $lang->language]]);
//
//            if ($permissionTranslationUpdate||$permissionTranslation || $permissions) {
//                DB::commit();
//                return response()->json(['ok' => true, "data" => ["permission" => $permission->first()->toArray(), "translations" => $permission->translations->toArray()]]);
//            }
//                DB::commit();
//                return response()->json(['ok' => true, "data" => ["permission" => $permission->first()->toArray(), "translations" => $permission->translations->toArray()]]);
//
//        } catch (\Exception $exception) {
//            DB::rollBack();
//        }
//        return response()->json(['ok' => false]);
    }



    public function destroy($id){
        $permission=Permission::destroy($id);
        return response()->json($permission);
    }


    public function inactive(Permission $permission){
        $permission = $permission->update([
            "status" => "inactive"
        ]);
        if ($permission) {
            return redirect()->route('management.permission.list')->with(["ok" => true]);
        }
        return redirect()->route('management.permission.list')->with(["ok" => false]);
    }


    public function active(Permission $permission){
        $permission = $permission->update([
            "status" => "active"
        ]);
        if ($permission) {
            return redirect()->route('management.permission.list')->with(["ok" => true]);
        }
        return redirect()->route('management.permission.list')->with(["ok" => false]);
    }
}
