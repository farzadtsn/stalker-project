<?php

namespace App\Http\Controllers\Management;

use App\Models\Tag;
use App\Models\TagTranslation;
use App\Models\Language;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

class TagController extends Controller{

    protected $kinds = [
        [
            "value" => 'news',
            "translation" => [
                "fa" => "خبر",
                "en" => "News"
            ],
        ],
        [
            "value" => 'event',
            "translation" => [
                "fa" => "رویداد",
                "en" => "Event"
            ],
        ]
    ];

    public function create()
    {
        $kinds = $this->kinds;
        return view('management.category.create' , compact('kinds'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'caption' => 'required|min:3|max:255|string',
            /*'value' => ['required','min:3','max:255','string','regex:/[a-z\/-0-9A-Z ]+/',Rule::unique('categories','value')->where(function ($query) {
                $query->orWhere("kind", "news")->orWhere("kind", "event");
            })],*/
            'kind' => 'required|in:news,event',
            'value' => ['min:3','max:255','string','regex:/[a-z\/-0-9A-Z ]+/',Rule::unique('categories','value')->where(function ($query) use($request) {
                if($request->kind) {
                    $query->where("kind" , $request->kind);
                } else {
                    $query->where("kind" , "unknown");
                }
            })],
        ]);
        $value = str_slug($request->value);
        DB::beginTransaction();
        try {
            $category = Category::create([
                'value' => $value ,
                'kind' => $request->kind ,
            ]);
            if ($category) {
                $categoryTranslation = CategoryTranslation::create([
                    'category_id' => $category->id ,
                    'caption' => $request->caption ,
                    'language' => env('LOCALE', 'fa') ,
                ]);
                if ($categoryTranslation) {
                    DB::commit();
                    return redirect()->route('management.category.list')->with(['ok' => true]);
                }
            }
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->route('management.category.create')->withInput()->withException($exception);
        }
        return redirect()->route('management.category.create')->with(['ok' => false]);
    }

    public function list()
    {
        $kinds = $this->kinds;
        $categories = Category::with(['translate'])->orWhere("kind" , "event")->orWhere("kind" , "news")->orderBy('id', 'desc')->get();
        return view('management.category.list', compact("categories" , "kinds"));
    }

    public function edit(Category $category)
    {
        return response()->json(['ok' => true, 'category' => $category->toArray(), 'translations' => $category->translations->toArray(), 'kinds' => $this->kinds]);
    }

    public function update(Category $category, $language, Request $request){
        $lang = false;
        if (is_numeric($language)) {
            $lang = Language::find($language);
        } else {
            $lang = Language::where([["language", $language], ["status", true]]);
        }
        if (!$lang) {
            abort(404);
        } else if ($lang->count() < 1) {
            abort(404);
        }
        $validator = validator($request->toArray(), [
            'caption' => 'required|min:3|max:255|string',
            'kind' => 'in:news,event',
            'status' => 'in:active,inactive,deleted',
            'value' => ['min:3','max:255','string','regex:/[a-z\/-0-9A-Z ]+/',Rule::unique('categories','value')->where("kind" , $request->kind)->ignore($category->id)],
        ]);
        if ($validator->fails()) {
            return response()->json(["ok" => false, "errors" => $validator->errors()]);
        }
        $lang = $lang->first();
        DB::beginTransaction();
        try {
            $categoryTranslationUpdate = false;
            $categoryTranslation = CategoryTranslation::where([['category_id', $category->id], ['language', $lang->language]]);
            if($categoryTranslation->count() > 0) {
                $categoryTranslationUpdate = $categoryTranslation->update([
                    'caption' => $request->caption,
                ]);
            }
            else {
                $categoryTranslation = CategoryTranslation::create([
                    "category_id" => $category->id,
                    "caption" => $request->caption,
                    "language" => $lang->language,
                ]);
            }
            if ($categoryTranslation || $categoryTranslationUpdate) {
                $fields = [];
                $updateCategory = false;
                if ($request->has("value")) {
                    $value = str_slug($request->value);
                    $fields["value"]  = $value;
                    $updateCategory = true;
                }
                if ($request->has("status")) {
                    $fields["status"] = $request->status;
                    $updateCategory = true;
                }
                if ($request->has("kind")) {
                    $fields["kind"] = $request->kind;
                    $updateCategory = true;
                }
                if ($updateCategory) {
                    $updateCategory = $category->update($fields);
                    if ($updateCategory) {
                        DB::commit();
                        return response()->json(['ok' => true, "data" => ["category" => $category->toArray(), "translations" => $category->translations->toArray()]]);
                    }
                } else {
                    DB::commit();
                    return response()->json(['ok' => true, "data" => ["category" => $category->toArray(), "translations" => $category->translations->toArray()]]);
                }
            }
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json($exception);
        }
        return response()->json(['ok' => false]);
    }

    public function delete(Category $category)
    {
        $category = $category->update([
            "status" => "deleted"
        ]);

        if ($category) {
            return redirect()->route('management.category.list')->with(["ok" => true]);
        }
        return redirect()->route('management.category.list')->with(["ok" => false]);
    }

    public function active(Category $category)
    {
        $category = $category->update([
            "status" => "active"
        ]);
        if ($category) {

            return redirect()->route('management.category.list')->with(["ok" => true]);
        }
        return redirect()->route('management.category.list')->with(["ok" => false]);
    }
}