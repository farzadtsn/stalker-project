<?php

namespace App\Http\Controllers\Management;

use App\Models\Language;
use App\Models\Role;
use App\Models\RoleTranslation;
use App\Models\Permission;
use Illuminate\Support\Facades\DB ;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RoleController extends Controller{

    public function list() {
//        dd(auth()->user()->hasRole(Permission::whereValue('All_Sections'))->first()->roles);
        $roles=Role::with(['translate'])->latest()->get();
        $permissions=Permission::latest()->get();
        return view('management.role.list',compact('roles','permissions'));
    }


    public function create() {
        $permissions=Permission::with(["translate"])->latest()->get();
        return view('management.role.create',compact('permissions'));
    }


    public function store(Request $request){

        $this->validate($request, [
            'caption' => 'required|min:2|max:255|string',
            'permission_id'=>'required',
            'value' => 'required|min:2|max:255|string',
        ]);

        DB::beginTransaction();
        try {
            $role = Role::create($request->all());
            $role->permissions()->sync($request->input('permission_id'));

            if ($role) {
                $roleTranslation =RoleTranslation::create([
                    "role_id" => $role->id,
                    "caption" => $request->caption,
                    "language" => "fa",
                ]);
                if($roleTranslation){
                    DB::commit();
                    return redirect()->route('management.role.list')->with(['ok' => true]);
                }
            }
        } catch (\Exception $exception) {
            DB::rollBack();
        }
        return redirect()->route('management.role.create')->with(["ok" => false]);
    }



    public function edit(Role $role){
        return response()->json(['ok' => true, 'role' => $role->toArray(), 'translations' => $role->translations->toArray()]);
    }



    public function update(Role $role , $language, Request $request){
        $lang = false;
        if (is_numeric($language)) {
            $lang = Language::find($language);
        } else {
            $lang = Language::where([["language", $language], ["status", true]]);
        }
        if (!$lang) {
            abort(404);
        } else if ($lang->count() < 1) {
            abort(404);
        }


        $validator = validator($request->toArray(), [
            'caption' => 'required|min:2|max:255|string',
            'permission_id' => 'required',
            'value' => 'required|min:2|max:255|string',
            'status' => 'in:active,inactive,deleted,blocked'
        ]);


        if ($validator->fails()) {
            return response()->json(["ok" => false, "errors" => $validator->errors()]);
        }

        $lang = $lang->first();
        DB::beginTransaction();
        try {
            $roleTranslationUpdate = false;
            $roleTranslation = RoleTranslation::where([['role_id', $role->id], ['language', $lang->language]]);
            if ($roleTranslation->count() > 0) {
                $roleTranslationUpdate = $roleTranslation->update([
                    'caption' => $request->caption,
                ]);
            } else {
                $roleTranslation = RoleTranslation::create([
                    "role_id" => $role->id,
                    "caption" => $request->caption,
                    "language" => $lang->language,
                ]);
            }

            if ($roleTranslation || $roleTranslationUpdate) {
                $fields = [];
                $updateRole = false;
                if ($request->has("value")) {
                    $value = str_slug($request->value);
                    $fields["value"] = $value;
                    $updateRole = true;
                }
                if ($request->has("status")) {
                    $fields["status"] = $request->status;
                    $updateRole = true;
                }
                if ($request->has("permission_id")) {
                    $roles = $role->permissions()->sync($request->input('permission_id'));
                    $fields["permission_id"] = $roles;
                    $updateRole = true;
                }
                if ($updateRole) {
                    $updateRole = $role->update($fields);
                    if ($updateRole) {
                        DB::commit();
                        return response()->json(['ok' => true, "data" => ["role" => $role->toArray(), "translations" => $role->translations->toArray()]]);
                    }
                } else {
                    DB::commit();
                    return response()->json(['ok' => true, "data" => ["role" => $role->toArray(), "translations" => $role->translations->toArray()]]);
                }
            }
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json($exception);
        }
        return response()->json(['ok' => false]);
    }


    public function inactive(Role $role){
        $role = $role->update([
            "status" => "inactive"
        ]);
        if ($role) {
            return redirect()->route('management.role.list')->with(["ok" => true]);
        }
        return redirect()->route('management.role.list')->with(["ok" => false]);
    }



    public function active(Role $role){
        $role = $role->update([
            "status" => "active"
        ]);
        if ($role) {
            return redirect()->route('management.role.list')->with(["ok" => true]);
        }
        return redirect()->route('management.role.list')->with(["ok" => false]);
    }
}
