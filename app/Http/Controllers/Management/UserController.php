<?php

namespace App\Http\Controllers\Management;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserController extends Controller{

    public function list(){
        $users = User::all();
        return response()->json(['ok' => true, 'users' => $users->toArray()]);
    }


    public function create(){
        return view('management.home');
    }


    public function store(Request $request){

        $this->validate($request , [
            'firstName'     => 'required|min:3|max:128|string',
            'lastName'      => 'required|min:3|max:128|string',
            'email'         => 'nullable|min:3|max:255|email|unique:users,email|string',
            'password'      => 'required|min:6|max:255',
            'mobileNumber'  => 'nullable|min:8|max:16|string',
            'introducing'   => 'required|min:3|max:255|string',
            'acquaintance'  => 'required|min:3|max:255|string',
            'level'         => 'required|in:user,admin'
        ]);

        DB::beginTransaction();
        try {
            $password = bcrypt($request->password);
            $users = User::create([
                "firstName"     => $request->firstName,
                "lastLame"      => $request->lastName,
                "email"         => $request->email,
                "password"      => $password ,
                "mobileNumber"  => $request->mobileNumber,
                "introducing"   => $request->introducing,
                "acquaintance"  => $request->acquaintance,
                "level"         => $request->level
            ]);
            if ($users) {
                DB::commit();
                return redirect()->route('management.panel',compact('users'))->with(["ok" => true]);
            }
        }catch (\Exception $exception){
            DB::rollBack();
        }
        return redirect(route('management.panel',compact('users'))->with(["ok" => false]));
    }


    public function edit(User $user){
        return response()->json(['ok' => true, 'user' => $user->toArray()]);
    }


    public function update(User $user , Request $request){

        $validator = validator( $request->toArray() , [
            'firstName'     => 'required|min:3|max:128|string',
            'lastName'      => 'required|min:3|max:128|string',
            'email'         => 'required|min:3|max:255|email|string',
            'password'      => 'required|min:6|max:255',
            'mobileNumber'  => 'nullable|min:8|max:16|string',
            'introducing'   => 'required|min:3|max:255|string',
            'acquaintance'  => 'required|min:3|max:255|string',
            'status'        => 'in:active,inactive,deleted,blocked',
            'level'         => 'required|in:user,admin'
        ]);

        if ($validator->fails()) {
            return response()->json(["ok" => false, "errors" => $validator->errors()]);
        }

        DB::beginTransaction();
        try {
            $fields = [];
            $updateUser = false;

            if ($request->has("firstName")) {
                $fields["firstName"]  = $request->firstName;
                $updateUser = true;
            }

            if ($request->has("lastName")) {
                $fields["lastName"] = $request->lastName;
                $updateUser = true;
            }

            if ($request->has("email")) {
                $fields["email"] = $request->email;
                $updateUser = true;
            }

            if ($request->has("password")) {
                $fields["password"] =bcrypt( $request->password);
                $updateUser = true;
            }

            if ($request->has("mobileNumber")) {
                $fields["mobileNumber"] = $request->mobileNumber;
                $updateUser = true;
            }

            if ($request->has("introducing")) {
                $fields["introducing"] = $request->introducing;
                $updateUser = true;
            }

            if ($request->has("acquaintance")) {
                $fields["acquaintance"] = $request->introducing;
                $updateUser = true;
            }

            if ($request->has("level")) {
                $fields["level"] = $request->level;
                $updateUser = true;
            }

            if ($request->has("status")) {
                $fields["status"] = $request->status;
                $updateUser = true;
            }

            if ($updateUser) {
                $updateUser = $user->update($fields);

                if ($updateUser) {
                    DB::commit();
                    return response()->json(['ok' => true, "data" => ["user" => $user->toArray()]]);
                }

            } else {
                DB::commit();
                return response()->json(['ok' => true, "data" => ["user" => $user->toArray()]]);
            }
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json($exception);
        }
        return response()->json(['ok' => false]);
    }


    public function inactive(User $user){
        $user = $user->update([
            "status" => "inactive"
        ]);
        if ($user) {
            return redirect()->route('management.panel')->with(["ok" => true]);
        }
        return redirect()->route('management.panel')->with(["ok" => false]);
    }


    public function active(User $user){
        $user = $user->update([
            "status" => "active"
        ]);
        if ($user) {
            return redirect()->route('management.panel')->with(["ok" => true]);
        }
        return redirect()->route('management.panel')->with(["ok" => false]);
    }

}
