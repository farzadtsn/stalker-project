<?php

namespace App\Http\Controllers\Management;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PanelController extends Controller{

    public function panel(){
        return view('management.panel');
    }
}
