<?php
use Illuminate\Support\Facades\Route;

/* ============================= Config System Route Group ===========================*/
Route::group(['namespace' => 'General'],function (){
    Route::get('/install',['as' =>'install.database','uses'=>'GeneralController@config']);
    Route::get('/language',['as' =>'language','uses'=>'LanguageController@change']);
    Route::get('/',['as' =>'home','uses'=>'GeneralController@home']);
});

/* =================================== Auth Route Group =================================*/
Route::group(['namespace' => 'Auth'], function () {
    Route::get('login', 'LoginController@showLoginForm')->name('login');
    Route::post('login', 'LoginController@login');
    Route::get('logout', 'LoginController@logout')->name('logout');
    Route::get('register', 'RegisterController@showRegistrationForm')->name('register');
    Route::post('register', 'RegisterController@register');
    Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('password/reset', 'ResetPasswordController@reset');
});

/* =================================== Management Route Group =================================*/
Route::group(['namespace' => 'Management','prefix'=> 'management','as'=>'management.'], function () {
    Route::get('/' , [
        'uses' => 'PanelController@panel',
        'as' => 'panel',
    ] );

    Route::get('/{any}' , [
        'uses' => 'PanelController@panel',
    ]);
});
