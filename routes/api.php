<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/* ========================== Management Route Group REST API ==============================*/
Route::group(['namespace' => 'Management','prefix'=> 'management','as'=>'api.management.'], function () {

    /* ======================= User Management Group Route ==========================*/
    Route::group(['prefix' => '/user','as'=>'user.'], function () {
        Route::get('/list', [
            'uses' => 'UserController@list',
            'as' => 'list',
        ]);

        Route::get('/create', [
            'uses' => 'UserController@create',
            'as' => 'create',
        ]);

        Route::post('/create', [
            'uses' => 'UserController@store',
        ]);

        Route::post('/active/{user}', [
            'uses' => 'UserController@active',
            'as' => 'active',
        ]);

        Route::post('/inactive/{user}', [
            'uses' => 'UserController@inactive',
            'as' => 'inactive',
        ]);
    });

});