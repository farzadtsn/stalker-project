<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration{

    public function up(){
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('firstName',128);
            $table->string('lastName',128);
            $table->string('email',255)->unique()->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('mobileNumber',16)->nullable();
            $table->string('country', 30)->nullable();
            $table->string('city', 50)->nullable();
            $table->string('birthDate', 25)->nullable();

            $table->integer('media_id')->unsigned()->comment('User Avatar');
            $table->foreign("media_id")->references("id")->on("medias");

            $table->enum('level',['user','admin'])->default('user');
            $table->enum('status',['active','inactive','deleted','blocked'])->default('active');
            $table->rememberToken();
            $table->timestamps();
        });
    }


    public function down(){
        Schema::disableForeignKeyConstraints();
            Schema::dropIfExists('users');
        Schema::enableForeignKeyConstraints();
    }
}
