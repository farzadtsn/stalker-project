<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMediasTable extends Migration{

    public function up(){
        Schema::create('medias', function (Blueprint $table) {
            $table->increments('id');
            $table->string("value" , 255);
            $table->string("postfix" , 16);
            $table->enum('kind', ['image' , 'video' , 'document' , 'audio']);
            $table->enum('type', ['cover' , 'logo' , 'avatar' , 'slider' , 'gallery'])->nullable();
            $table->enum('status', ['active' , 'inactive' , 'deleted'])->default('active');

            $table->integer("category_id")->unsigned();
            $table->foreign("category_id")->references("id")->on("categories");

            $table->integer('model_id')->unsigned()->nullable();
            $table->string('model_type' , 255)->nullable();
            $table->index(["model_id" ,"model_type"]);

            $table->timestamps();
        });
    }


    public function down(){
        Schema::disableForeignKeyConstraints();
            Schema::dropIfExists('medias');
        Schema::enableForeignKeyConstraints();
    }
}
