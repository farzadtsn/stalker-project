<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTranslationsTable extends Migration{

    public function up(){
        //    A B C D E F G H I J K L M N O P Q R S T U V W X Y Z
		
		   Schema::create('role_translation', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('role_id')->unsigned();
            $table->string('caption',255);
            $table->string('language',2)->nullable(false);
            $table->timestamps();
            $table->unique(["role_id" , "language"]);
            $table->foreign("role_id")->references("id")->on("roles");
        });

        Schema::create('permission_translation', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('permission_id')->unsigned();
            $table->string('caption',255);
            $table->string('language',2)->nullable(false);
            $table->timestamps();
            $table->unique(["permission_id" , "language"]);
            $table->foreign("permission_id")->references("id")->on("permissions");
        });

        Schema::create('category_translation', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id')->unsigned();
            $table->foreign("category_id")->references("id")->on("categories");
            $table->string('caption',255);
            $table->string('language',2)->nullable(false);
            $table->unique(["category_id" , "language"]);
            $table->timestamps();
        });

        Schema::create('tag_translation', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tag_id')->unsigned();
            $table->foreign("tag_id")->references("id")->on("tags");
            $table->string('caption',255);
            $table->string('language',2)->nullable(false);
            $table->unique(["tag_id" , "language"]);
            $table->timestamps();
        });


        Schema::create('media_translation', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('media_id')->unsigned();
            $table->foreign("media_id")->references("id")->on("medias");
            $table->string('caption' , 255);
            $table->string('description' , 1024);
            $table->string('language',2)->nullable(false);
            $table->unique(["media_id" , "language"]);
            $table->timestamps();
        });

    }


    public function down(){
        Schema::disableForeignKeyConstraints();
			Schema::dropIfExists('role_translation');
			Schema::dropIfExists('permission_translation');
            Schema::dropIfExists('category_translation');
            Schema::dropIfExists('media_translation');
        Schema::enableForeignKeyConstraints();
    }
}
