<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration{

    public function up(){
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string("value" , 255)->nullable(true);
            $table->enum("kind" , ["media" , "news" , "event"]);
            $table->enum("status", ["active" , "inactive" , "deleted"])->default("active");
            $table->timestamps();
        });
    }


    public function down(){
        Schema::disableForeignKeyConstraints();
            Schema::dropIfExists('categories');
        Schema::enableForeignKeyConstraints();
    }
}
