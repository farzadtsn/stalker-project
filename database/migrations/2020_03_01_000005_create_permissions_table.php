<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermissionsTable extends Migration{

    public function up(){
        Schema::create('permissions', function (Blueprint $table) {
            $table->increments('id');
            $table->string("value" , 255);
            $table->enum("status", ["active" , "inactive" , "deleted" , "blocked"])->default("active");
            $table->timestamps();
        });
    }


    public function down(){
        Schema::disableForeignKeyConstraints();
            Schema::dropIfExists('permissions');
        Schema::enableForeignKeyConstraints();
    }
}
