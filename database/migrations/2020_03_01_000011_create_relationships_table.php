<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelationshipsTable extends Migration{

    public function up(){
        //    A B C D E F G H I J K L M N O P Q R S T U V W X Y Z

      Schema::create('role_user', function (Blueprint $table) {
            $table->integer('role_id')->unsigned();
            $table->foreign('role_id')->references('id')->on('roles');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->primary(['role_id' , 'user_id']);
        });


        Schema::create('permission_role', function (Blueprint $table) {
            $table->integer('role_id')->unsigned();
            $table->foreign('role_id')->references('id')->on('roles');
            $table->integer('permission_id')->unsigned();
            $table->foreign('permission_id')->references('id')->on('permissions');
            $table->primary(['role_id' , 'permission_id']);
        });

    }




    public function down(){
        Schema::disableForeignKeyConstraints();
			Schema::dropIfExists('role_user');
            Schema::dropIfExists('permission_role');
        Schema::enableForeignKeyConstraints();
    }
}
