<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLanguagesTable extends Migration{

    public function up(){
        Schema::create('languages', function (Blueprint $table) {
            $table->increments('id');
            $table->string("caption" ,255)->nullable(false);
            $table->string("language" ,2)->unique();
            $table->enum("status", ["active" , "inactive"])->default("active");
            $table->timestamps();
        });
    }


    public function down(){
        Schema::disableForeignKeyConstraints();
            Schema::dropIfExists('languages');
        Schema::enableForeignKeyConstraints();
    }
}
