<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder{

    public function run(){
    /*------------------------------- Config System Settings --------------------------------------*/
        $languages = [
            ['caption' => 'فارسی', 'language' => 'fa','created_at' => Carbon::now()->subWeeks(3),'updated_at' => Carbon::now()],
            ['caption' => 'english', 'language' => 'en','created_at' => Carbon::now()->subDays(3),'updated_at' => Carbon::now()],
        ];
        foreach($languages as $language){
            DB::table('languages')->insert($language);
        }

    /* ================================  Users Section Data Seeder =============================*/
        $users = [
            ['id'=> 1 ,'firstName' => 'پوریا','lastName' => 'زرین فر','email'=>'zarrinFar@gmail.com','password'=>bcrypt('123456'),'mobileNumber'=>'09140002500' ,'country'=>'ایران','city'=>'تهران','birthDate' => '1360/05/09','level'=>'user','status'=>'active','created_at' => Carbon::now()->subWeeks(3)],
            ['id'=> 2 ,'firstName' => 'مجید','lastName' => 'آریان نژاد','email'=>'aryanZezhad@gmail.com','password'=>bcrypt('123456'),'mobileNumber'=>'09120002200' ,'country'=>'کانادا','city'=>'تورنتو','birthDate' => '1365/01/02','level'=>'admin','status'=>'inactive','created_at' => Carbon::now()->subDays(9)],
            ['id'=> 3 ,'firstName' => 'فرزاد','lastName' => 'اسماعیلی','email'=>'esmaeili@gmail.com','password'=>bcrypt('123456'),'mobileNumber'=>'09140002852' ,'country'=>'کانادا','city'=>'ونکوور','birthDate' => '1370/08/12','level'=>'admin','status'=>'active','created_at' => Carbon::now()->subMonths(2)],
        ];
        foreach ($users as $user) {
            DB::table('users')->insert($user);
        }
    }
}
